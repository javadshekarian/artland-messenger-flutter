class Helper {
  _Helper(){}
  static Helper shared = Helper();

  String devidePhoneNumber(phoneNumber){
    return '${phoneNumber!.substring(0,3)} ${phoneNumber!.substring(3,6)} ${phoneNumber!.substring(6,9)} ${phoneNumber!.substring(9,phoneNumber.length)}';
  }

  String findFileTemp(fileName){
    // picker.files.first.name
    var splitFileName = fileName.split('.');
    String fileTemp = splitFileName[splitFileName.length - 1];
    return fileTemp;
  }
}