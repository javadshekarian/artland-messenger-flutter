import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:messenger2/addContact/addContact.dart';
import 'package:messenger2/channel/channel.dart';
import 'package:messenger2/chatroom/chatroom.dart';
import 'package:messenger2/config/config.dart';
import 'package:messenger2/group/group.dart';
import 'package:messenger2/helper/helper.dart';
import 'package:messenger2/register/register.dart';
import 'package:messenger2/setting/setting.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;

class HomeView{
  _HomeView(){}
  static HomeView shared = HomeView();
  static const _kFontFam = 'MyFlutterApp';
  static const String? _kFontPkg = null;
  Helper _helper = Helper();

  Drawer homeDrawer(
      BuildContext context,
      String? profilePicture,
      String? phoneNumber,
      String? username,
      http.Response? loadingResponse
      ){
    return Drawer(
      width: MediaQuery.of(context).size.width/1.5,
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          Container(
            padding: const EdgeInsets.only(top: 30,bottom: 30),
            color: Colors.blue,
            child: Row(
              children: [
                const SizedBox(width: 10,),
                CircleAvatar(
                  backgroundImage: (profilePicture != null && profilePicture != '')?
                  NetworkImage('${Config.shared.baseUrl}/$profilePicture'):
                  NetworkImage('${Config.shared.baseUrl}/profileLogo.png'),

                  radius: 30,
                ),
                const SizedBox(width: 10,),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      (username == null)?'':username,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 14
                      ),
                    ),
                    SizedBox(height: 3,),
                    Text(
                      (phoneNumber == null)?
                          _helper.devidePhoneNumber('+111111111111'):
                          _helper.devidePhoneNumber(phoneNumber),
                      style: const TextStyle(
                        fontSize: 12
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
          ListTile(
            dense: true,
            leading: const Icon(
              Icons.person_add,
              size: 22,
            ),
            title: const Text(
              'add contact',
              style: TextStyle(
                fontSize: 14
              ),
            ),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => AddContactPage())
              );
            },
          ),
          ListTile(
            leading: const Icon(
              IconData(0xe153, fontFamily: 'MaterialIcons'),
              size: 20,
            ),
            title: const Text(
              'create group',
              style: TextStyle(
                fontSize: 14
              ),
            ),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => const GroupPage())
              );
            },
          ),
          ListTile(
            leading: const Icon(
              Icons.alt_route
            ),
            title: const Text(
              'create channel',
              style: TextStyle(
                fontSize: 14
              ),
            ),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => const ChannelPage())
              );
            },
          ),
          ListTile(
            leading: const Icon(
                Icons.bookmark_border
            ),
            title: const Text(
              'save message',
              style: TextStyle(
                fontSize: 14
              ),
            ),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => ChatroomPage(
                    contactName: jsonDecode(loadingResponse!.body)["data"][0]["contactID"]["username"],
                    contactPhone: jsonDecode(loadingResponse.body)["data"][0]["contactID"]["phoneNumber"],
                    profilePicture:
                    (jsonDecode(loadingResponse.body)["data"][0]["contactID"]["profilePicture"].length == 0)?
                    'profileLogo.png':
                    jsonDecode(loadingResponse.body)["data"][0]["contactID"]["profilePicture"]
                    ,
                    contactID: jsonDecode(loadingResponse.body)["data"][0]["contactID"]["_id"]
                ))
              );
            },
          ),
          ListTile(
            dense: true,
            leading: const Icon(
              Icons.settings,
              size: 22,
            ),
            title: const Text(
              'setting',
              style: TextStyle(
                  fontSize: 14
              ),
            ),
            onTap: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => const SettingPage())
              );
            },
          ),
          const SizedBox(height: 10,),
          Container(
            height: 1,
            color: Colors.black26,
          ),
          const SizedBox(height: 10,),
          ListTile(
            leading: const Icon(
              Icons.logout,
              size: 22,
            ),
            title: const Text(
              'logout',
              style: TextStyle(
                  fontSize: 14
              ),
            ),
            onTap: ()async{
              var localCache = await SharedPreferences.getInstance();
              localCache.setString('token', '');
              
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => RegisterPage())
              );
            },
          ),
          ListTile(
            leading: const Icon(
              Icons.question_mark,
              size: 22,
            ),
            title: const Text(
              'about me!',
              style: TextStyle(
                  fontSize: 14
              ),
            ),
            onTap: ()async{
              _launchURL();
            },
          ),
        ],
      ),
    );
  }

  _launchURL() async {
    const url = 'https://bychat.one';
    if (await canLaunchUrl(Uri.parse(url))) {
      await launchUrl(Uri.parse(url));
    } else {
      throw 'Could not launch $url';
    }
  }
}