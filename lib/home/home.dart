import 'dart:convert';
import 'package:messenger2/chatroom/chatroom.dart';

import 'package:flutter/material.dart';
import 'package:messenger2/config/config.dart';
import 'package:messenger2/home/view.dart';
import 'package:messenger2/register/register.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:socket_io_client/socket_io_client.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  
  final HomeView _homeView = HomeView();
  http.Response? loadingResponse;
  late Socket socket;
  String? profilePicture;
  Map<String,dynamic>? accountData;

  @override
  void initState() {
    super.initState();
    _queryAccountData();
    _queryContacts();
    _initSocket();
  }

  _queryAccountData()async{
    var localCache = await SharedPreferences.getInstance();
    var token = localCache.getString('token');
    var response = await http.post(
        Uri.parse('${Config.shared.baseUrl}/login/query-account-data'),
        body: {
          "token":token
        }
    );

    setState(() {
      accountData = jsonDecode(response.body);
    });
  }

  _queryContacts()async{
    var localCache = await SharedPreferences.getInstance();
    var token = localCache.getString('token');
    var response = await http.post(
      Uri.parse('${Config.shared.baseUrl}/contact/query-contact'),
      body: {
        "token":token
      }
    );

    setState(() {
      loadingResponse = response;
      profilePicture = localCache.getString('profilePicture');
    });
  }

  _initSocket()async{
    String token = await getToken();
    socket = io(
        Config.shared.baseUrl,
        OptionBuilder()
            .setTransports(["websocket"])
            .disableAutoConnect()
            .enableForceNew()
            .build()
    );
    socket.onConnect((data){
      socket.emit("online",{
        "token":token
      });
    });

    // socket.onDisconnect((data) => print('onDisconnect: $data'));
    // socket.onError((error) => print('onError: $error'));

    socket.connect();
  }

  Future<String> getToken()async{
    var localCache = await SharedPreferences.getInstance();
    String? token = localCache.getString('token');
    if(token != '' && token != null){
      return token;
    }else{
      Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => RegisterPage())
      );
      return '';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      drawer: (accountData == null || loadingResponse == null)?
      _homeView.homeDrawer(
          context,
          null,
          null,
          null,
          null
      ):_homeView.homeDrawer(
          context,
          accountData!["profilePicture"],
          accountData!["phoneNumber"],
          accountData!["username"],
          loadingResponse
      ),

      body: NestedScrollView(
        headerSliverBuilder: (context, hasScrolled) {
          return [
            const SliverAppBar(
              backgroundColor: Colors.cyan,
              collapsedHeight: 65,
              title: Text(
                'Artland',
                style: TextStyle(
                  fontFamily: 'englishBroken',
                  fontWeight: FontWeight.bold,
                  fontSize: 24
                ),
              ),
              pinned: true,
              centerTitle: true,
            )
          ];
        },
        body: (loadingResponse == null)?
        const Center(
          child: SizedBox(
            height: 50.0,
            width: 50.0,
            child: CircularProgressIndicator(
              color: Colors.blue,
            ),
          ),
        ):
        ListView(
          children: [
            for(int i=0;i<jsonDecode(loadingResponse!.body)["data"].length;++i)
              Container(
                  decoration: const BoxDecoration(
                      color: Colors.green,
                      border: Border(
                          top: BorderSide(
                              width: 1,
                              color: Colors.black26
                          )
                      )
                  ),
                  child: TextButton(
                    onPressed: (){
                      Navigator.of(context).push(
                          MaterialPageRoute(
                              builder: (context) => ChatroomPage(
                                  contactName: jsonDecode(loadingResponse!.body)["data"][i]["contactID"]["username"],
                                  contactPhone: jsonDecode(loadingResponse!.body)["data"][i]["contactID"]["phoneNumber"],
                                  profilePicture:
                                  (jsonDecode(loadingResponse!.body)["data"][i]["contactID"]["profilePicture"].length == 0)?
                                  'profileLogo.png':
                                  jsonDecode(loadingResponse!.body)["data"][i]["contactID"]["profilePicture"]
                                  ,
                                  contactID: jsonDecode(loadingResponse!.body)["data"][i]["contactID"]["_id"]
                              )
                          )
                      );
                    },
                    style: ButtonStyle(
                      backgroundColor: const MaterialStatePropertyAll(
                          Colors.white
                      ),
                      padding: const MaterialStatePropertyAll(
                          EdgeInsets.only(top: 15,bottom: 15)
                      ),
                      shape: MaterialStatePropertyAll(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(0)
                          )
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            const SizedBox(width: 5,),
                            CircleAvatar(
                              backgroundImage: (jsonDecode(loadingResponse!.body)["data"][i]["contactID"]["profilePicture"].length == 0)?
                              NetworkImage('${Config.shared.baseUrl}/profileLogo.png'):
                              NetworkImage(
                                  '${Config.shared.baseUrl}/${jsonDecode(loadingResponse!.body)["data"][i]["contactID"]["profilePicture"]}'
                              )
                              ,
                            ),
                            const SizedBox(width: 10,),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  jsonDecode(loadingResponse!.body)["data"][i]["contactName"],
                                  style: const TextStyle(
                                      fontFamily: "robotoRegular",
                                      color: Colors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                                const Text(
                                  'hello! how are you?! can you speak?',
                                  style: TextStyle(
                                      fontFamily: "robotoRegular",
                                      color: Colors.black87,
                                      fontSize: 12
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                        const Icon(
                          IconData(0xe156, fontFamily: 'MaterialIcons'),
                          size: 20,
                          color: Colors.green,
                        )
                      ],
                    ),
                  )
              )
          ],
        ),
      ),
    );
  }
}
