import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:messenger2/addContact/model.dart';
import 'package:messenger2/addContact/view.dart';
import 'package:http/http.dart' as http;
import 'package:messenger2/config/config.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:alert_dialog/alert_dialog.dart';

class AddContactPage extends StatefulWidget {
  const AddContactPage({super.key});

  @override
  State<AddContactPage> createState() => _AddContactPageState();
}

class _AddContactPageState extends State<AddContactPage> {

  final AddContactView _addContactView = AddContactView();
  String? app_token;

  @override
  void initState() {
    super.initState();
    _setTokenValue();
  }

  _setTokenValue()async{
    var localCache = await SharedPreferences.getInstance();
    var token = localCache.getString('token');
    app_token = token;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TextButton(
                style: const ButtonStyle(
                  overlayColor: MaterialStatePropertyAll(
                    Colors.transparent
                  ),
                ),
                onPressed: (){
                  Navigator.pop(context);
                },
                child: Container(
                  margin: const EdgeInsets.only(left: 5),
                  child: const Icon(
                    Icons.arrow_back,
                    color: Colors.black,
                  ),
                ),
              ),
              const Text(
                'Add Contact',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold
                ),
              ),
              const Text('   ')
            ],
          ),
          const SizedBox(height: 15,),
          _addContactView.addContactInput(
              context,
              'Enter Contact Name',
              AddContactModel.shared.contactName,
              false
          ),
          const SizedBox(height: 10,),
          _addContactView.addContactInput(
              context,
              'Enter Contact Phone Number',
              AddContactModel.shared.contactPhone,
              false
          ),
          const SizedBox(height: 10,),
          TextButton(
            style: const ButtonStyle(
                backgroundColor: MaterialStatePropertyAll(
                    Colors.blue
                ),
                padding: MaterialStatePropertyAll(
                    EdgeInsets.all(15)
                ),
                shape: MaterialStatePropertyAll(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                            Radius.circular(010)
                        )
                    )
                )
            ),
            onPressed: ()async{
              var contactName = AddContactModel.shared.contactName.text;
              var contactPhone = AddContactModel.shared.contactPhone.text;

              var response = await http.post(
                Uri.parse('${Config.shared.baseUrl}/contact/create-contact'),
                body: {
                  "contactName":contactName,
                  "contactPhone":contactPhone,
                  "token":app_token
                }
              );

              switch(response.statusCode){
                case 202:
                  showDialog(
                      context: context,
                      builder: (context){
                        return const AlertDialog(
                          backgroundColor: Colors.green,
                          contentPadding: EdgeInsets.all(20),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                              side: BorderSide(
                                  width: 4,
                                  color: Colors.black26
                              )
                          ),
                          content: Text(
                            'contact is created!',
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold
                            ),
                          ),
                        );
                      }
                  );
                  break;
                case 403:
                  var data = jsonDecode(response.body)["data"];
                  alert(context,title: Text(
                    data,
                    style: const TextStyle(
                        fontSize: 16
                    ),
                  ));
                  break;
                case 404:
                  var data = jsonDecode(response.body)["data"];
                  alert(context,title: Text(
                    data,
                    style: const TextStyle(
                        fontSize: 16
                    ),
                  ));
                  break;
                case 500:
                  alert(context,title: const Text(
                    "Server Error",
                    style: TextStyle(
                        fontSize: 16
                    ),
                  ));
                  break;
              }

            },
            child: const Text(
              'add contact',
              style: TextStyle(
                  color: Colors.white
              ),
            ),
          ),
        ],
      ),
    );
  }
}
