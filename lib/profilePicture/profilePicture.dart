import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:messenger2/config/config.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class ProfilePicturePage extends StatefulWidget {
  String contactID;
  ProfilePicturePage({
    super.key,
    required this.contactID
  });

  @override
  State<ProfilePicturePage> createState() => _ProfilePicturePageState();
}

class _ProfilePicturePageState extends State<ProfilePicturePage> {

  String? profilePicture;

  @override
  void initState() {
    super.initState();
    _queryCurrentUserInformation();
  }

  _queryCurrentUserInformation()async{
    Uri uri = Uri.parse('${Config.shared.baseUrl}/login/query-with-id');
    var response = await http.post(uri,body: {
      "user_id":widget.contactID
    });

    setState(() {
      profilePicture = jsonDecode(response.body)["profilePicture"];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: const BoxDecoration(
          color: Colors.black
        ),
        child: Align(
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: (profilePicture == null)?
                    NetworkImage('${Config.shared.baseUrl}/03nXhtAjv.jpg'):
                    NetworkImage('${Config.shared.baseUrl}/$profilePicture'),

                    filterQuality: FilterQuality.high,
                    fit: BoxFit.contain
                  )
                ),
              ),
              Align(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextButton(
                        onPressed: (){

                        },
                        child: Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                              color: const Color.fromARGB(170, 0, 0, 0),
                              borderRadius: BorderRadius.circular(50)
                          ),
                          child: const Icon(
                            Icons.arrow_back,
                            size: 25,
                            color: Colors.white,
                          ),
                        )
                    ),
                    TextButton(
                        onPressed: (){

                        },
                        child: Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                              color: const Color.fromARGB(170, 0, 0, 0),
                              borderRadius: BorderRadius.circular(50)
                          ),
                          child: const Icon(
                            Icons.arrow_forward,
                            size: 25,
                            color: Colors.white,
                          ),
                        )
                    ),
                  ],
                ),
              ),
              TextButton(
                  onPressed: (){
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    margin: const EdgeInsets.only(top: 10),
                    child: const Icon(
                      Icons.arrow_back,
                      color: Color.fromARGB(255, 100, 100, 100),
                      size: 24,
                    ),
                  )
              ),
            ],
          ),
        ),
      ),
    );
  }
}
