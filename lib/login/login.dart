import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:messenger2/config/config.dart';
import 'package:messenger2/home/home.dart';
import 'package:messenger2/register/register.dart';
import './view.dart';
import './model.dart';
import 'package:http/http.dart' as http;
import 'package:alert_dialog/alert_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  final LoginView _loginView = LoginView();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Text(
            'Login',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Colors.black54
            ),
          ),
          const SizedBox(height: 10,),
          _loginView.loginInput(context, 'Enter Your Username', LoginModel.shared.username,false)
          ,
          const SizedBox(height: 10,),
          _loginView.loginInput(context, 'Enter  Your Password', LoginModel.shared.password,true)
          ,
          Align(
            alignment: Alignment.topLeft,
            child: TextButton(
                onPressed: (){
                  Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (context) => const RegisterPage()
                    )
                  );
                },
                child: const Text(
                    'register'
                )
            ),
          ),
          const SizedBox(height: 10,),
          TextButton(
              onPressed: ()async{
                String username = LoginModel.shared.username.text;
                String password = LoginModel.shared.password.text;
                
                var response = await http.post(
                  Uri.parse('${Config.shared.baseUrl}/login/login'),
                  body: {
                    "username":username,
                    "password":password
                  }
                );

                var data = jsonDecode(response.body);

                switch(response.statusCode){
                  case 500:
                    alert(context, title:const Text(
                      "Server Error",
                      style: TextStyle(
                          fontSize: 16
                      ),
                    ));
                    break;
                  case 404:
                    alert(context, title:Text(
                      data["data"],
                      style: const TextStyle(
                          fontSize: 16
                      ),
                    ));
                    break;
                  case 203:
                    alert(context, title:Text(
                      data["data"],
                      style: const TextStyle(
                          fontSize: 16
                      ),
                    ));
                    break;
                  case 200:
                    var token = jsonDecode(response.body)["token"];

                    setToken(token,context);
                    showDialog(
                        context: context,
                        builder: (context){
                          return const AlertDialog(
                            backgroundColor: Colors.green,
                            contentPadding: EdgeInsets.all(20),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(10)),
                                side: BorderSide(
                                    width: 4,
                                    color: Colors.black26
                                )
                            ),
                            content: Text(
                              'You Are Login!',
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          );
                        }
                    );
                    break;
                }
              },
              style: ButtonStyle(
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)
                  )
                ),
                backgroundColor: MaterialStateProperty.all(
                  Colors.blue
                ),
                padding: MaterialStateProperty.all(
                  const EdgeInsets.all(15)
                ),
              ),
              child: const Text(
                'login',
                style: TextStyle(
                  color: Colors.white
                ),
              )
          ),
        ],
      ),
    );
  }

  setToken(String token,BuildContext context)async{
    var localCache = await SharedPreferences.getInstance();
    localCache.setString('token', token);
    await Future.delayed(
        const Duration(seconds: 2)
    );
    Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => const HomePage())
    );
  }
}
