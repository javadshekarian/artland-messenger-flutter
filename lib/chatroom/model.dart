import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ChatroomModel{
  _ChatroomModel(){}
  static ChatroomModel shared = ChatroomModel();

  TextEditingController messageInput = TextEditingController();
  FocusNode messageInputFocus = FocusNode();
  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool notification = true;
  PageController pageviewJumpTo = PageController();
}