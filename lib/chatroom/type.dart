class Contact {
  String? contactName;
  String? contactPhone;
  String? username;
  String? contactID;


  Contact({
    this.contactName,
    this.contactPhone,
    this.username,
    this.contactID
  });

  factory Contact.fromJson(Map<String, dynamic> json) => Contact(
      contactName: json["contactName"],
      contactPhone: json["contactPhone"],
      username: json["username"],
      contactID: json["contactID"]
  );

  Map<String, dynamic> toJson() => {
    "contactName": contactName,
    "contactPhone": contactPhone,
    "username": username,
    "contactID": contactID
  };
}