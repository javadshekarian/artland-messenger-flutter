import 'dart:convert';
import 'dart:ui';

import 'package:alert_dialog/alert_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:image_picker/image_picker.dart';
import 'package:messenger2/chatroom/model.dart';
import 'package:messenger2/config/config.dart';
import 'package:messenger2/profilePicture/profilePicture.dart';
import 'package:file_picker/file_picker.dart';
import '../upload/uploadFile.dart';
import 'package:alert_dialog/alert_dialog.dart';

class ChatRoomView{

  Widget receiveMessage(Map<String,dynamic> messageIndex){
    return Card(
        elevation: 0,
        color: Colors.transparent,
        margin: const EdgeInsets.only(right: 100),
        child: Padding(
          padding: const EdgeInsets.only(top: 10,left: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.tealAccent
                ),
                padding: const EdgeInsets.only(top: 5,bottom: 5,right: 15,left: 15),
                child: Text(
                  messageIndex["message"]
                ),
              )
            ],
          ),
        )
    );
  }

  PreferredSize appbar(
      String profilePicture,
      String contactName,
      String phoneNumber,
      BuildContext context
      ){
    return PreferredSize(
      preferredSize: const Size.fromHeight(60),
      child: GestureDetector(
        onTap: (){
          ChatroomModel.shared.scaffoldKey.currentState!.openEndDrawer();
        },
        child: AppBar(
          iconTheme: const IconThemeData(
            size: 0
          ),
          automaticallyImplyLeading: false,
          backgroundColor: const Color.fromARGB(255, 240, 240, 240),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              TextButton(
                style: const ButtonStyle(
                  iconSize:MaterialStatePropertyAll(
                    24
                  ),
                ),
                onPressed: (){
                  Navigator.of(context).pop();
                },
                child: const Icon(
                  Icons.arrow_back,
                  color: Colors.black,
                ),
              ),
              CircleAvatar(
                backgroundImage: (profilePicture != null)?
                NetworkImage('${Config.shared.baseUrl}/$profilePicture'):
                NetworkImage('${Config.shared.baseUrl}/profileLogo.png'),
                radius: 23,
              ),
              const SizedBox(width: 10,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    contactName,
                    style: const TextStyle(
                        fontSize: 14,
                        color: Colors.black54,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'robotoRegular'
                    ),
                  ),
                  const SizedBox(height: 2,),
                  Text(
                    phoneNumber,
                    style: const TextStyle(
                        fontSize: 12,
                        color: Colors.black54,
                        fontFamily: 'robotoRegular'
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget sendMessage(Map<String,dynamic> messageIndex){
    return Card(
        elevation: 0,
        color: Colors.transparent,
        margin: const EdgeInsets.only(left: 100),
        child: Padding(
          padding: const EdgeInsets.only(top: 10,right: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.orangeAccent
                ),
                padding: const EdgeInsets.only(top: 5,bottom: 5,right: 15,left: 15),
                child: Text(
                  messageIndex["message"]
                ),
              )
            ],
          ),
        )
    );
  }

  InputDecoration messageInputDecoration(){
    return InputDecoration(
        enabledBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(0),
              bottomRight: Radius.circular(0)
            ),
            borderSide: BorderSide(
              width: 0,
              color: Colors.transparent,
            )
        ),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(0),
            borderSide: const BorderSide(
              width: 0,
              color: Colors.transparent,
            )
        ),
        hintText: 'message ...',
        contentPadding: const EdgeInsets.only(top: 14, bottom: 14, left: 10, right: 10),
        isDense: true,
    );
  }

  TextStyle profileTextStyle(
      double fs,
      FontWeight fw
      ){
    return TextStyle(
        color: Colors.white,
        fontWeight: fw,
        fontSize: fs,
        fontFamily: 'robotoRegular'
    );
  }

  Widget messageViewDrawerHeader(
      BuildContext context,
      String profilePicture,
      String contactName,
      String contactID
      ){
    return Stack(
      children: [
        Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: 60,
              color: Colors.blue,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: const EdgeInsets.only(left: 5),
                    child: TextButton(
                      onPressed: (){
                        ChatroomModel.shared.scaffoldKey.currentState!.openDrawer();
                      },
                      child: const Icon(
                        Icons.arrow_back,
                        size: 26,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Container(
                    width: 120,
                    child: const Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Icon(
                          Icons.videocam,
                          color: Colors.white,
                        ),
                        Icon(
                          Icons.call,
                          color: Colors.white,
                        ),
                        Icon(
                          Icons.more_vert_outlined,
                          color: Colors.white,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 80,
              color: Colors.blue,
              child: TextButton(
                onPressed: (){
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => ProfilePicturePage(
                      contactID:contactID
                    ))
                  );
                },
                style: ButtonStyle(
                  overlayColor: MaterialStateColor.resolveWith((states) => Colors.transparent),
                ),
                child: Row(
                  children: [
                    const SizedBox(width: 10,),
                    CircleAvatar(
                      backgroundImage: NetworkImage(
                          '${Config.shared.baseUrl}/${profilePicture}'
                      ),
                      radius: 30,
                    ),
                    const SizedBox(width: 10,),
                    Container(
                      height: 40,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            contactName,
                            style: profileTextStyle(
                                14,
                                FontWeight.bold
                            ),
                          ),
                          Text(
                            'last seen reacently',
                            style: profileTextStyle(
                                12,
                                FontWeight.normal
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
        Container(
          height: 160,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    width: 45,
                    height: 45,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(50),
                        boxShadow: const [
                          BoxShadow(
                              color: Colors.black38,
                              offset: Offset(-2,2),
                              blurRadius: 3
                          )
                        ]
                    ),
                    child: TextButton(
                      style: ButtonStyle(
                        overlayColor: MaterialStateColor.resolveWith((states) => Colors.transparent),
                      ),
                      onPressed: (){
                        ChatroomModel.shared.scaffoldKey.currentState!.openDrawer();
                      },
                      child: const Icon(
                        Icons.message_outlined,
                        size: 20,
                        color: Colors.black54,
                      ),
                    ),
                  ),
                  const SizedBox(width: 10,),
                ],
              )
            ],
          ),
        )
      ],
    );
  }

  Widget messageViewInformation(
      String txt,
      Color clr,
      FontWeight fw,
      double fs
      ){
    return Container(
      margin: const EdgeInsets.only(left: 5),
      child: Text(
        txt,
        style: TextStyle(
            color: clr,
            fontFamily: 'robotoRegular',
            fontWeight: fw,
            fontSize: fs
        ),
      ),
    );
  }

  Widget uploadFileInMessageInput(
      String? token,
      String? contactID,
      BuildContext context,
      String contactName,
      {required Function(Map<String,dynamic>) function}
      ){

    return SizedBox(
      child: TextButton(
        onPressed: ()async{
          var picker = await FilePicker.platform.pickFiles();

          if (picker != null) {
            String url = '${Config.shared.baseUrl}/message/send-file';
            var response = await UploadFile.shared.uploadFile(
                url,
                picker.files.first.path!,
                token,
                contactID,
                contactName
            );

            switch(response.statusCode){
              case 200:
                function(jsonDecode(response.body));
                break;
              case 406:
                alert(
                  context,
                  title: const Text(
                     'File Not Acceptable!'
                  )
                );
                break;
              case 422:
                alert(
                  context,
                  title: const Text(
                     'Upload Error!'
                  )
                );
                break;
              case 500:
                alert(
                  context,
                  title: const Text(
                    'Server Error!'
                  )
                );
                break;
            }
          }
        },
        style: messageInputButtonStyle(),
        child: Transform.rotate(
          angle: -45,
          child: const Icon(
            IconData(0xe0b3, fontFamily: 'MaterialIcons'),
            size: 26,
            color: Colors.black54,
          ),
        ),
      ),
    );
  }

  ButtonStyle messageInputButtonStyle(){
    return ButtonStyle(
      shape: MaterialStatePropertyAll(
          RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0)
          )
      ),
      padding: const MaterialStatePropertyAll(
          EdgeInsets.only(
            right: 0
          )
      ),
      overlayColor: MaterialStateProperty.all(Colors.transparent),
      splashFactory: NoSplash.splashFactory,
    );
  }

  Widget sendVoiceInMessageInput(){
    return SizedBox(
      width: 30,
      child: TextButton(
        onPressed: (){

        },
        style: messageInputButtonStyle(),
        child: const Icon(
          Icons.add,
          size: 26,
          color: Colors.black54,
        )
      ),
    );
  }
}