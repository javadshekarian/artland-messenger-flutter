import 'dart:async';
import 'dart:convert';
import 'dart:ui';
import 'package:alert_dialog/alert_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:messenger2/config/config.dart';
import 'package:messenger2/login/login.dart';
import 'package:messenger2/register/register.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import './model.dart';
import 'package:flutter/material.dart';
import './view.dart';
import 'package:socket_io_client/socket_io_client.dart';
import 'package:jwt_decoder/jwt_decoder.dart';

class ChatroomPage extends StatefulWidget {
  String contactName;
  String contactPhone;
  String profilePicture;
  String contactID;


  ChatroomPage({
    super.key,
    required this.contactName,
    required this.contactPhone,
    required this.profilePicture,
    required this.contactID
  });

  @override
  State<ChatroomPage> createState() => _ChatroomPageState();
}

class _ChatroomPageState extends State<ChatroomPage> {

  final ChatRoomView _chatRoomView = ChatRoomView();
  final _streamController = StreamController<Map<String,dynamic>>();
  final ScrollController _scrollController = ScrollController();
  late Socket socket;
  String? app_token;
  final List<Map<String,dynamic>> _chat = [];
  String? profilePicture;
  Map<String,dynamic>? myAccount;
  bool isSwitched = false;
  Color outlineTrackColorCustom = Colors.black26;
  bool _isUpload = true;
  bool _isPlay = false;

  @override
  void initState(){
    super.initState();
    _queryContactInformation();
    _checkUserToken();
    _initSocket();
    _queryMessages();
  }

  _queryContactInformation()async{
    Uri uri = Uri.parse('${Config.shared.baseUrl}/contact/query-current-contact');
    var localCache = await SharedPreferences.getInstance();
    String? token = localCache.getString('token');
    var decodedToken = JwtDecoder.decode(token!);

    var response = await http.post(uri,body:{
      "username":decodedToken["username"],
      "contactID":widget.contactID
    });

    setState(() {
      isSwitched = jsonDecode(response.body)["notification"];
    });
  }

  _queryMessages()async {
    Uri uri = Uri.parse('${Config.shared.baseUrl}/message/query-messages');
    var localCache = await SharedPreferences.getInstance();
    String? token = localCache.getString('token');
    var decodedToken = JwtDecoder.decode(token!);


    if(decodedToken["username"] == widget.contactName){
      var response = await http.post(uri,body:{
        "username":decodedToken["username"],
        "contactID":widget.contactID,
        "isSaveMessage":"yes"
      });

      for(int i=0;i<jsonDecode(response.body).length;++i){
        setState(() {
          _chat.add(jsonDecode(response.body)[i]);
          _streamController.sink.add(jsonDecode(response.body)[i]);
        });
      }
    }else{
      var response = await http.post(uri,body:{
        "username":decodedToken["username"],
        "contactID":widget.contactID,
        "isSaveMessage":"no"
      });

      for(int i=0;i<jsonDecode(response.body).length;++i){
        setState(() {
          _chat.add(jsonDecode(response.body)[i]);
          _streamController.sink.add(jsonDecode(response.body)[i]);
        });
      }
    }
  }

  _checkUserToken()async{
    var localCache = await SharedPreferences.getInstance();
    var token = localCache.getString('token');
    setState(() {
      profilePicture = localCache.getString('profilePicture');
      app_token = token;
      myAccount = JwtDecoder.decode(token!);
    });
    var response = await http.post(
      Uri.parse('${Config.shared.baseUrl}/login/check-token'),
      body: {
        "token":token
      }
    );

    switch(response.statusCode){
      case 500:
      case 400:
        Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => LoginPage())
        );
        break;
      case 200:
        break;
    }
  }

  _initSocket(){
    socket = io(
      Config.shared.baseUrl,
      OptionBuilder()
        .setTransports(["websocket"])
        .disableAutoConnect()
        .enableForceNew()
        .build()
    );
    // socket.onConnect((data) => print('you are connected!'));
    // socket.onDisconnect((data) => print('onDisconnect: $data'));
    // socket.onError((error) => print('onError: $error'));
    socket.on("fail",(data){
      Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => RegisterPage())
      );
    });

    socket.on("catch-error",(data){
      alert(context, title: const Text(
        'Server Error',
        style: TextStyle(
            fontSize: 16
        ),
      ));
    });

    socket.on('message', (data){
      _chat.add(data);
      _streamController.sink.add(data);
    });

    socket.on('send-file',(data){
      _chat.add(data);
      _streamController.sink.add(data);
    });

    socket.connect();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: ChatroomModel.shared.scaffoldKey,
      appBar: _chatRoomView.appbar(
        widget.profilePicture,
        widget.contactName,
        widget.contactPhone,
        context
      ),
      endDrawer: Drawer(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(0)
        ),
        width: MediaQuery.of(context).size.width,
        child: ListView(
          children: [
            _chatRoomView.messageViewDrawerHeader(
                context,
                widget.profilePicture,
                widget.contactName,
                widget.contactID
            ),
            _chatRoomView.messageViewInformation(
                'info:',
                Colors.blue,
                FontWeight.bold,
                14
            ),
            const SizedBox(height: 5,),
            _chatRoomView.messageViewInformation(
                'phone number:',
                Colors.black87,
                FontWeight.normal,
                12
            ),
            _chatRoomView.messageViewInformation(
                widget.contactPhone,
                Colors.black,
                FontWeight.bold,
                14
            ),
            const SizedBox(height: 10,),
            _chatRoomView.messageViewInformation(
                'username:',
                Colors.black87,
                FontWeight.normal,
                12
            ),
            _chatRoomView.messageViewInformation(
                widget.contactName,
                Colors.black,
                FontWeight.bold,
                14
            ),
            const SizedBox(height: 10,),
            const Divider(
              color: Colors.black12,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  margin: const EdgeInsets.only(left: 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'Notification',
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                      Text(
                        (isSwitched == true)?'turn of':'turn on',
                        style: const TextStyle(
                          fontSize: 12
                        ),
                      )
                    ],
                  )
                ),
                Transform.scale(
                  scale: 0.75,
                  child: Switch(
                    value: isSwitched,
                    thumbColor: const MaterialStatePropertyAll(
                        Colors.blue
                    ),
                    inactiveTrackColor: Colors.black12,
                    trackOutlineColor: MaterialStatePropertyAll(
                        outlineTrackColorCustom
                    ),
                    trackColor: const MaterialStatePropertyAll(
                        Colors.black12
                    ),
                    onChanged: (value)async{
                      setState(() {
                        isSwitched = value;
                        outlineTrackColorCustom = value?Colors.blue:Colors.black26;
                      });

                      Uri uri = Uri.parse('${Config.shared.baseUrl}/contact/update-notification');
                      var response = await http.post(uri,body: {
                        "notification":value?"on":"of",
                        "token":app_token,
                        "contactID":widget.contactID
                      });

                      if(response.statusCode == 500){
                        alert(
                          context,
                          title: const Text(
                            'Server Error'
                          )
                        );
                      }
                    },
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  ),
                ),

              ],
            ),
            const SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                TextButton(
                    onPressed: (){
                      ChatroomModel.shared.pageviewJumpTo.animateToPage(
                          0,
                          duration: const Duration(milliseconds: 500),
                          curve: Curves.easeInOut
                      );
                    },
                    child: const Text(
                      'links',
                      style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'robotoRegular',
                        fontSize: 16
                      ),
                    )
                ),
                TextButton(
                    onPressed: (){
                      ChatroomModel.shared.pageviewJumpTo.animateToPage(
                          1,
                          duration: const Duration(milliseconds: 500),
                          curve: Curves.easeInOut
                      );
                    },
                    child: const Text(
                      'files',
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'robotoRegular',
                          fontSize: 16
                      ),
                    )
                ),
                TextButton(
                    onPressed: (){
                      ChatroomModel.shared.pageviewJumpTo.animateToPage(
                          2,
                          duration: const Duration(milliseconds: 500),
                          curve: Curves.easeInOut
                      );
                    },
                    child: const Text(
                      'voices',
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'robotoRegular',
                          fontSize: 16
                      ),
                    )
                ),
              ],
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: PageView(
                
                controller: ChatroomModel.shared.pageviewJumpTo,
                children: [
                  Container(
                    margin: const EdgeInsets.only(left: 5),
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    child: ListView(
                      children: [
                        for(int i=0;i<30;++i)
                          Container(
                            margin: const EdgeInsets.only(top: 10),
                            child: const Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'some explain about link'
                                ),
                                Text(
                                  'https://bychat.one',
                                  style: TextStyle(
                                      color: Colors.blue,
                                      decoration: TextDecoration.underline,
                                      decorationColor: Colors.blue
                                  ),
                                )
                              ],
                            ),
                          )
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    child: ListView(
                      children: [
                        for(int i=0;i<50;++i)
                          Container(
                            margin: const EdgeInsets.only(top: 10),
                            child: const Row(
                              children: [
                                Icon(
                                  Icons.insert_drive_file_outlined,
                                  size: 28,
                                ),
                                SizedBox(width: 5,),
                                Text(
                                  'fileName.pdf',
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: 'robotoRegular'
                                  ),
                                )
                              ],
                            ),
                          )
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    child: ListView(
                      children: [
                        for(int i=0;i<50;++i)
                          Container(
                            margin: const EdgeInsets.only(top: 2),
                            child: Row(
                              children: [
                                Container(
                                  margin: const EdgeInsets.only(left: 5),
                                  child: const Icon(
                                    Icons.play_circle,
                                    color: Colors.blue,
                                    size: 40,
                                  ),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 5),
                                  child: const Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        '25 march 2024',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold
                                        ),
                                      ),
                                      Text(
                                        'singerName',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black54
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        )
      ),
      body: Center(
        child: Column(
          children: [
            Expanded(
              child: StreamBuilder(
                stream: _streamController.stream,
                builder: (context,snapshot){
                  return ListView.builder(
                    controller: _scrollController,
                    itemCount: _chat.length,
                    itemBuilder: (context,index){
                      return Card(
                        elevation: 0,
                        color: Colors.transparent,
                        margin: const EdgeInsets.only(left: 70),
                        child: Container(
                          margin: EdgeInsets.only(top: 5,right: 1),
                          width: MediaQuery.of(context).size.width*0.8,
                          height: 65,
                          decoration: BoxDecoration(
                            color: Colors.black12,
                            borderRadius: BorderRadius.circular(20)
                          ),
                          child: Row(
                            children: [
                              SizedBox(
                                width: 55,
                                height: 55,
                                child: TextButton(
                                  style: ButtonStyle(
                                    overlayColor: MaterialStateProperty.all(Colors.transparent)
                                  ),
                                  onPressed: (){
                                    setState(() {
                                      _isPlay = !_isPlay;
                                    });
                                  },
                                  child: Icon(
                                    _isPlay?Icons.pause_circle:Icons.play_circle,
                                    size: 55,
                                    color: Color.fromARGB(255, 0, 200, 255),
                                  ),
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(left: 12),
                                width:MediaQuery.of(context).size.width - 150,
                                height: 3,
                                color: Colors.red,
                              )
                            ],
                          ),
                        ),
                      );
                      // if(_chat[index]["sender"] == myAccount!["username"]){
                      //   return _chatRoomView.sendMessage(
                      //       _chat[index]
                      //   );
                      // }else{
                      //   return _chatRoomView.receiveMessage(
                      //       _chat[index]
                      //   );
                      // }
                    },
                  );
                },
              ),
            ),
            const SizedBox(height: 5),
            messageInput(),
          ],
        ),
      ),
    );
  }

  Widget messageInput(){
    return SizedBox(
      height: 45,
      width: MediaQuery.of(context).size.width - 10,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width - 45,
            child: Container(
              decoration: const BoxDecoration(
                border: Border(
                  top: BorderSide(
                    width: 1,
                    color: Colors.black26
                  ),
                  bottom: BorderSide(
                    width: 1,
                    color: Colors.black26
                  ),
                  left: BorderSide(
                    width: 1,
                    color: Colors.black26
                  )
                )
              ),
              child:  TextField(
                autofocus: true,
                focusNode: ChatroomModel.shared.messageInputFocus,
                onSubmitted: (value) async {

                  Map<String,dynamic> myMessage = {
                    "message":value,
                    "contactID":widget.contactID,
                    "receiver":widget.contactName,
                    "sender":myAccount!["username"]
                  };

                  _chat.add(myMessage);
                  _streamController.sink.add(myMessage);

                  var decodedToken = JwtDecoder.decode(app_token!);

                  if(widget.contactName == decodedToken["username"]){
                    Uri uri = Uri.parse('${Config.shared.baseUrl}/message/save-message');

                    var response = await http.post(uri,body: {
                      "message":value,
                      "username":decodedToken["username"],
                      "contactID":decodedToken["user_id"],
                      "expireTime":decodedToken["exp"].toString()
                    });

                    switch(response.statusCode){
                      case 202:
                        break;
                      case 500:
                        alert(
                            context,
                            title: const Text(
                                'Server Error!'
                            )
                        );
                        break;
                      case 406:
                        var localCache = await SharedPreferences.getInstance();
                        localCache.setString('token', '');

                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (context) => LoginPage())
                        );
                        break;
                    }
                  }else{
                    socket.emit('message',{
                      "message":value,
                      "token":app_token,
                      "contactName":widget.contactName,
                      "contactPhone":widget.contactPhone,
                      "contactID":widget.contactID
                    });
                  }

                  ChatroomModel.shared.messageInput.clear();
                  ChatroomModel.shared.messageInputFocus.requestFocus();

                  _scrollController.animateTo(
                      _scrollController.position.maxScrollExtent + 55,
                      duration: const Duration(milliseconds: 250),
                      curve: Curves.easeInOut
                  );

                },
                controller: ChatroomModel.shared.messageInput,
                cursorColor: Colors.black26,
                style: const TextStyle(
                  height: 1,
                ),
                decoration: _chatRoomView.messageInputDecoration(),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(right: 5, left: 5, top: 2, bottom: 2),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(
                  width: 1,
                  color: Colors.black26
                ),
                right: BorderSide(
                  width: 1,
                  color: Colors.black26
                ),
                bottom: BorderSide(
                  width: 1,
                  color: Colors.black26
                )
              ),
            ),
            child: SizedBox(
              width: 24,
              child: GestureDetector(
                onPanEnd: (details) {
                  setState(() {
                    _isUpload = !_isUpload;
                  });
                },
                child: (_isUpload && (app_token != null) && (widget.contactID != null))?
                _chatRoomView.uploadFileInMessageInput(
                  app_token!,
                  widget.contactID,
                  context,
                  widget.contactName,
                  function: (Map<String,dynamic> message){
                    setState(() {
                      _chat.add(message);
                      _streamController.sink.add(message);
                    });

                    socket.emit('send-file',{
                      "fileName":message["message"],
                      "sender":message["sender"],
                      "contactName":widget.contactName,
                      "contactID":message["contactID"],
                      "messageType":message["messageType"]
                    });
                  }
                ):
                _chatRoomView.sendVoiceInMessageInput(),
              ),
            ),
          )
        ],
      )
    );
  }
}
