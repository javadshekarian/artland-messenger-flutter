import 'package:flutter/material.dart';

class RegisterView{
  _RegisterView(){}
  static RegisterView shared = RegisterView();

  Widget loginInput(
      BuildContext context,
      String hintTextContent,
      TextEditingController textEditingController,
      bool isSecure
      ){
    return Center(
      child: SizedBox(
        width: MediaQuery.of(context).size.width - 20,
        child: TextField(
          controller: textEditingController,
          obscureText: isSecure,
          style: const TextStyle(
              fontSize: 14
          ),
          decoration: InputDecoration(
              hintText: hintTextContent,
              hintStyle: const TextStyle(
                  fontSize: 14
              ),
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                    width: 1,
                    color: Colors.black54
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  width: 1,
                  color: Colors.black54,
                ),
              ),
              isDense: true,
              contentPadding: const EdgeInsets.only(
                  top: 13,
                  bottom: 13,
                  left: 10,
                  right: 10
              )
          ),
        ),
      ),
    );
  }
}