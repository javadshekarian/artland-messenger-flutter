import 'dart:convert';
import 'package:messenger2/config/config.dart';
import 'package:messenger2/home/home.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:alert_dialog/alert_dialog.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';

class RegisterController{
  _RegisterController(){}
  static RegisterController shared = RegisterController();

  setValueInCache(
      String email,
      String username
      )async{
    var localCache = await SharedPreferences.getInstance();
    localCache.setString('APP_EMAIL', email);
    localCache.setString('APP_USERNAME', username);
  }

  Future<String> register(
      String username,
      String password,
      String email,
      BuildContext context,
      String phoneNumber
      )async{
    var params = {
      "username":username,
      "password":password,
      "email":email,
      "phoneNumber":phoneNumber
    };

    var response = await http.post(
      Uri.parse('${Config.shared.baseUrl}/login/register'),
      body: params
    );

    if(response.statusCode == 500){
      alert(context, title: const Text(
        'Server Error',
        style: TextStyle(
          fontSize: 16
        ),
      ));
    }else if(response.statusCode == 409){
      Map<String,dynamic> data = jsonDecode(response.body);
      alert(context, title:Text(
          data["data"],
        style: const TextStyle(
          fontSize: 16
        ),
      ));
    }else if(response.statusCode == 400){
      var error = jsonDecode(response.body)["data"];
      alert(context,title: Text(
        error,
        style: const TextStyle(
          fontSize: 16
        ),
      ));
    }

    if(response.statusCode != 202){
      return '';
    }else{
      return jsonDecode(response.body)["token"];
    }
  }

  youAreRegisteredAlertDialog(
      BuildContext context
      ){
    return showDialog(
        context: context,
        builder: (context){
          return const AlertDialog(
            backgroundColor: Colors.green,
            contentPadding: EdgeInsets.all(20),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                side: BorderSide(
                    width: 4,
                    color: Colors.black26
                )
            ),
            content: Text(
              'You Are Registered!',
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold
              ),
            ),
          );
        }
    );
  }

  setToken(String token,BuildContext context)async{

    youAreRegisteredAlertDialog(context);

    var localCache = await SharedPreferences.getInstance();
    localCache.setString('token', token);
    await Future.delayed(
      const Duration(seconds: 1)
    );
    Navigator.of(context).push(
      MaterialPageRoute(builder: (context) => const HomePage())
    );
  }

  uploadAvatar()async{
    final ImagePicker picker = ImagePicker();
    final XFile? image = await picker.pickImage(
        source: ImageSource.gallery
    );
    if(image == null){
      return;
    }
  }
}