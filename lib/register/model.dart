import 'package:flutter/cupertino.dart';

class RegisterModel{
  _RegisterModel(){}
  static RegisterModel shared = RegisterModel();

  TextEditingController username = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController repeatPassword = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController phoneNumber = TextEditingController();
}