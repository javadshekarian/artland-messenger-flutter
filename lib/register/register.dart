import 'package:alert_dialog/alert_dialog.dart';
import 'package:flutter/material.dart';
import 'package:messenger2/login/login.dart';
import 'package:messenger2/register/controller.dart';
import 'package:messenger2/register/model.dart';
import 'package:messenger2/register/view.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

import 'package:messenger2/upload/uploadFile.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  
  final RegisterView _registerView = RegisterView();
  final UploadFile _uploadFile = UploadFile();
  File _profileAvatar = File('');
  
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Text(
            'Register'
          ),
          const SizedBox(height: 10,),
          _registerView.loginInput(context, 'Enter Your Email', RegisterModel.shared.email,false),
          const SizedBox(height: 10,),
          _registerView.loginInput(context, 'Enter Your Username',RegisterModel.shared.username,false),
          const SizedBox(height: 10,),
          _registerView.loginInput(context, 'Enter Your Phone Number', RegisterModel.shared.phoneNumber,false),
          const SizedBox(height: 10,),
          _registerView.loginInput(context, 'Enter Your Password', RegisterModel.shared.password,true),
          const SizedBox(height: 10,),
          _registerView.loginInput(context, 'Enter Your Password Again', RegisterModel.shared.repeatPassword,true),
          const SizedBox(height: 10,),
          TextButton(
            style: ButtonStyle(
              fixedSize: MaterialStateProperty.all(
                Size.fromWidth(MediaQuery.of(context).size.width - 20)
              ),
              backgroundColor: MaterialStateProperty.all(
                Colors.blue
              ),
              padding: MaterialStateProperty.all(
                const EdgeInsets.only(top: 18,bottom: 18)
              ),
              shape: MaterialStateProperty.all(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)
                )
              )
            ),
            onPressed: ()async{
              final ImagePicker picker = ImagePicker();
              final XFile? image = await picker.pickImage(
                  source: ImageSource.gallery
              );
              if(image == null){
                return;
              }
              File profilePicture = File(image.path);
              _profileAvatar = profilePicture;
            },
            child: const Text(
              'Select Profile Picture',
              style: TextStyle(
                color: Colors.white
              ),
            ),
          ),
          const SizedBox(height: 5,),
          Align(
            alignment: Alignment.topLeft,
            child: TextButton(
                onPressed: (){
                  Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (context) => const LoginPage()
                      )
                  );
                },
                child: const Text('login')
            ),
          ),
          const SizedBox(height: 10,),
          TextButton(
              onPressed: ()async{
                String email = RegisterModel.shared.email.text;
                String username = RegisterModel.shared.username.text;
                String password = RegisterModel.shared.password.text;
                String repeatPassword = RegisterModel.shared.repeatPassword.text;
                String phoneNumber = RegisterModel.shared.phoneNumber.text;

                if(password == repeatPassword){
                  String token = await RegisterController.shared.register(
                      username,
                      password,
                      email,
                      context,
                      phoneNumber
                  );
                  if(_profileAvatar.path != ''){
                    String fileName = await _uploadFile.uploadProfilePicture(
                      _profileAvatar,
                      token
                    );

                    var localCache = await SharedPreferences.getInstance();
                    localCache.setString('profilePicture', fileName);
                  }

                  if(token.isNotEmpty){
                    RegisterController.shared.setToken(token, context);
                  }

                }else{
                  alert(context,title:
                    const Text(
                      'your passwords are different!',
                      style: TextStyle(
                        fontSize: 16
                      ),
                    )
                  );
                }
              },
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(
                  Colors.blue
                ),
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)
                  )
                ),
                side: MaterialStateProperty.all(
                  const BorderSide(
                    width: 1,
                    color: Colors.black45
                  )
                )
              ),
              child: const Text(
                'register',
                style: TextStyle(
                  color: Colors.white
                ),
              )
          )
        ],
      ),
    );
  }
}
