import 'package:shared_preferences/shared_preferences.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class Config{
  _Config(){}
  static Config shared = Config();

  final String baseUrl = 'http://localhost:3003';

  Future<bool> setupMainRoute()async{
    var localCache = await SharedPreferences.getInstance();
    var token = localCache.getString('token');

    Uri uri = Uri.parse('$baseUrl/login/check-token');
    var response = await http.post(
      uri,
      body: {
        "token":token
      }
    );

    if(response.statusCode == 200){
      return true;
    }else{
      return false;
    }
  }
}