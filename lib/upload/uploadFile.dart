import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:io';

import 'package:messenger2/config/config.dart';

class UploadFile{
  _UploadFile(){}
  static UploadFile shared = UploadFile();

  Future<String> uploadProfilePicture(File profilePicture,String token)async{
    var request = http.MultipartRequest(
        'post',
        Uri.parse(
            '${Config.shared.baseUrl}/upload/profile-picture'
        )
    );

    request.fields["token"] = token;
    request.files.add(await http.MultipartFile.fromPath('profilePicture', profilePicture.path));
    final streamedResponse = await request.send();
    final response = await http.Response.fromStream(streamedResponse);
    String fileName = jsonDecode(response.body)["data"];

    return fileName;
  }

  Future<http.Response> uploadFile(
      String url,
      String filePath,
      String? token,
      String? contactID,
      String? contactName
      ) async {
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.fields["token"] = token!;
    request.fields["contactID"] = contactID!;
    request.fields["contactName"] = contactName!;
    request.files.add(await http.MultipartFile.fromPath('file', filePath));

    final streamedResponse = await request.send();
    final response = await http.Response.fromStream(streamedResponse);
    
    return response;
  }
}