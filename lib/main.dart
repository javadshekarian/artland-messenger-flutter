import 'package:flutter/material.dart';
import 'package:messenger2/config/config.dart';
import 'package:messenger2/home/home.dart';
import './login/login.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MainRoute(),
    );
  }
}

class MainRoute extends StatefulWidget {
  const MainRoute({super.key});

  @override
  State<MainRoute> createState() => _MainRouteState();
}

class _MainRouteState extends State<MainRoute> {

  @override
  void initState() {
    super.initState();
    _initMainPage();
  }

  _initMainPage()async{
    var result = await Config.shared.setupMainRoute();
    if(result){
      Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => HomePage())
      );
    }else{
      Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => LoginPage())
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return const LoginPage();
  }
}

